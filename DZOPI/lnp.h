#ifndef lnp_H
#define lnp_H

#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>

using namespace std;

void input(const char* filename){
    int id;
    string name;
    int cost;

    cout << "Input information about plant\n";

    cout << "Id: ";
    cin >> id;

    cout << "Name: ";
    cin >> name;

    cout << "Cost: ";
    cin >> cost;

    ofstream fout(filename, ios_base::app);
    fout << id << " " << name << " " << cost << "\n";
}

struct store_item{
    string name;
    int cost;
    int id;
};

#endif;
