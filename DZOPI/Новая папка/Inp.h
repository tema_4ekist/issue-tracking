#ifndef lnp_H
#define lnp_H

#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>

void input(const char* filename){
    string name;
    int cost;

    cout << "Input information about plant\n";

    cout << "Name: ";
    cin >> name;

    cout << "Cost: ";
    cin >> cost;

    ofstream fout(filename);
    fout << name << " " << cost << "\n";
}


#endif;
