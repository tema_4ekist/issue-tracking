﻿#include <iostream>
#include <string>
#include <fstream>
#include <windows.h>
#include <conio.h>
#include "lnp.h"
#include "Control.h"
#include <map>

using namespace std;


const int N = 4;
string Menu[N];
int length[N];
string* MenuPointer = Menu;
char Move;
class FileS
{
	public:
	string NameFile;
	void create(int _id, string _name, int _cost)
	{
		ofstream fout("plants.txt");
		fout << _id << ' ' << _name << ' ' << _cost << endl;
		fout.close();
	}
};

class Product
{
public:
	int Id;
	string Name;
	int Cost;
	Product(int _id, string _name, int _cost)
	{
		Id = _id;
		Name = _name;
		Cost = _cost;
	}

	~Product()	{};




	void ShowProduct()
	{
		cout << " Id " << Id << " Name " << Name << " Cost " << Cost << endl;
	}

	void AddProduct(int _id, string _name, int _cost)
	{
		FileS e;
		e.create(_id, _name, _cost);

	}
};

void ChangeProduct()
	{
	    store_item produ;
	    cout << "Input Id: ";
	    cin >> produ.id;

	    cout << "Input new name: ";
	    cin >> produ.name;

	    cout << "Input new cost: ";
	    cin >> produ.cost;

	    ifstream fin("data.dat");

	    map <int, pair <string, int>> prod;
	    int Iden;
	    pair <string, int> Information;
	    while(fin >> Iden >> Information.first >> Information.second) prod[Iden] = Information;

	    prod[produ.id] = {produ.name, produ.cost};

	    fin.close();

        ofstream fout("data.dat");
        for(auto it : prod){
            pair <string, int> Info = it.second;
            fout << it.first << " " << Info.first << " " << Info.second << "\n";
        }
        fout.close();
	}

void PrintProducts(string NameFile)
{
	string s;
	ifstream fin(NameFile);
	while (getline(fin, s))
	{
		cout << s << endl;
	}
	fin.close();
}

int main()
{

	Menu[0] = "Input";
	Menu[1] = "Output";
	Menu[2] = "Change";
	Menu[3] = "Exit";

	for (int i = 0; i < N; i++) length[i] = Menu[i].size();

    Output(MenuPointer, N);
    gotoxy(length[0] + 6, positionY);
	while (true) {
        Move = _getch();
        if (Move == -32) {
            Move = _getch();
            switch (Move) {
            case 80:
                if (positionY < 22) MoveToDown(length[positionY - 18], positionY);
                break;
            case 72:
                if (positionY > 19) MoveToUp(length[positionY - 20], positionY);
                break;
            }
            continue;
        }
        if (Move == 13) {
            char Enter;
            system("cls");
            switch (positionY) {
            case 19:
                gotoxy(0, 0);
                input("data.dat");
                break;
            case 20:
                PrintProducts("data.dat");
                Enter = _getch();
                positionY = 19;
                break;
            case 21:
                {

                ChangeProduct();
                Enter = _getch();
                positionY = 19;
                }
                break;
            case 22:
                return 0;
            }
            system("cls");
            gotoxy(length[0] + 6, positionY);
        }
        Output(MenuPointer, N);
        gotoxy(length[0] + 6, positionY);
	}
}

